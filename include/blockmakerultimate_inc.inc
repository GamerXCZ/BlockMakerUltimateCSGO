#if defined _blockmakerultimate_inc_included
 #endinput
#endif
#define _blockmakerultimate_inc_included

//forward to detect your 3rd party object touch and code its action
forward void BM_OnBlockTouch(int iBlock, int iClient, blockTypes BlockType);
//native to register new object type. use on plugin start
native BM_Register_BlockType(blockTypes yourBlockType, char sBlockName[96], char sPath[96]);
//use in onblocktouch forward to detect blocktype: (blocktype, BLOCK_BHOP)
#define IsBlockType(%1,%2) (%1&%2)