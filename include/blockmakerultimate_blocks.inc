#if defined _blockmakerultimate_blocks_included
 #endinput
#endif
#define _blockmakerultimate_blocks_included

//define max block entities supported by mod
#define MaxBlocks 1536

//ADD new objects here before registering your 3rd party plugin
enum blockTypes(<<=1){
	BLOCK_NONE = 0x00001,
	BLOCK_PLATFORM,
	BLOCK_BHOP,

	BLOCK_MAX = 3 //increase by 1 when adding new objects to calculate bitsum when needed
};
