public Plugin:myinfo = { name = "The BlockMaker: Ultimate", author = "diablix", description = "", version = "0.2", url = "none atm" }

#include <sourcemod>
#include <sdktools>
#include <sdkhooks>
#include <blockmakerultimate_blocks>
#include "BMUlt\core\messaging.sp"
#include "BMUlt\blocks\blockdata.sp"
#include "BMUlt\blocks\blocktypes.sp"
#include "BMUlt\blocks\hooks.sp"
#include "BMUlt\blocks\utils.sp"
#include "BMUlt\blocks\stocks.sp"
#include "BMUlt\user\hooks.sp"

public OnMapStart(){
	blockData_ResetAllBlocks();
}

public void OnPluginStart(){
	g_FWBlockTouchedForward = new GlobalForward("BM_OnBlockTouch", view_as<ExecType>(0x03), view_as<ParamType>(0x02), view_as<ParamType>(0x02), view_as<ParamType>(0x0));

	RegConsoleCmd("say", _hookSay);
}

public APLRes AskPluginLoad2(Handle hMyself, bool bLate, char[] sError, int iErrMax){
	CreateNative("BM_Register_BlockType", _nativeBlockTypeRegister);
	return view_as<APLRes>(0x0);
}