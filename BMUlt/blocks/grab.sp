
enum struct BM_GrabPhysicsData{
	int iGrabbing;
	float fGrabDistance;
	float fGrabOffset[3];
	float fEntOrigin[3];
	float fPlayerOrigin[3];
	float fAimPlace[3];
	float fEyePos[3];
	float fEyeAng[3];
	float fFinalBlockPos[3];
	
	void reset(){
		this.iGrabbing=-1;
	}
	
	void Block_SetGrabbed(int iBlock){
		this.iGrabbing=iBlock;
	}
	void Block_BeingGrabbed(float fEyesPos[3], float fEyesAng[3]){
		if(this.iGrabbing==-1){
			for(int i=0;i<3;i++){
				this.fEyePos[i]=fEyesPos[i];
				this.fEyeAng[i]=fEyesAng[i];
			}
		}
	}
	void Block_GoingToMoved(int iClient, float fEntOri[3], float fPlayerOri[3], float fPlayerAng[3], float fMovedVecPos[3]){
	}
}

BM_GrabPhysicsData 		g_Grabber[MAXPLAYERS+1];

stock AddInFrontOf(iClient, Float:vecOrigin[3], Float:vecAngle[3], Float:units, Float:output[3]){
	new Float:vecAngVectors[3];
	vecAngVectors = vecAngle;
	GetAngleVectors(vecAngVectors, vecAngVectors, NULL_VECTOR, NULL_VECTOR);
	for (new i; i < 3; i++)
		output[i] += vecOrigin[i] + g_Grabber[iClient].fGrabOffset[i] + (vecAngVectors[i] * units);
}