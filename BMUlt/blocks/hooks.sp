GlobalForward g_FWBlockTouchedForward;

public _hook_TouchBlock(int iBlock, int iClient){
	if(!(1 <= iClient <= MaxClients)) return 0;
	#if defined LOGCHAT
	BM_BlockDataChatlog.messageChat(0, "\x01%d\x10 bitsum", Block[iBlock].BlockType);
	#endif

	Call_StartForward(g_FWBlockTouchedForward);
	Call_PushCell(iBlock);
	Call_PushCell(iClient);
	Call_PushCell(Block[iBlock].BlockType);
	Call_Finish();
	return 0x0;
}