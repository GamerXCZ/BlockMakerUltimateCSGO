BM_ChatMessage BM_BlockDataChatlog;
enum blockShapes{ BLOCK_POLE, BLOCK_SMALL, BLOCK_NORMAL, BLOCK_LARGE };
enum struct BM_BlockDataSet {
	int Entity;
	char BaseMdlPath[256];
	blockTypes BlockType;
	blockShapes BlockSize;
	
	int BlockCreate(int iClient, int iPreSpawnedEntity, blockTypes createdBlockType, blockShapes createdBlockShape){
		this.BlockTypeReset();
		this.Entity=iPreSpawnedEntity;
		this.BlockTypeAdd(createdBlockType);
		this.BlockSize=createdBlockShape;
		
		blockTypes_getModelPathByBlockType(createdBlockType, this.BaseMdlPath, 256);
		util_BlockSpawn_AtAim(iClient, this.Entity, this.BaseMdlPath);

		#if defined LOGCHAT
		BM_BlockDataChatlog.messageChat(0, "Spawned \x01%d\x10 (bitsum \x01%d\x10)(blocksize \x01%d\x10)[\x01%s\x10]", view_as<int>(createdBlockType), this.BlockType, this.BlockSize, this.BaseMdlPath);
		#endif
		return this.Entity;
	}
	void BlockRemove(){
		util_BlockRemove(this.Entity);
		this.BlockTypeReset();
	}
	blockTypes BlockTypeConvert(blockTypes blockTypeToConvert){
		#if defined LOGCHAT
		blockTypes oldType = this.BlockType;
		BM_BlockDataChatlog.messageChat(0, "Converted \x01%d\x10 ->\x01 %d\x10(new bitsum \x01%d\x10)", oldType, blockTypeToConvert, this.BlockType);
		#endif
		this.BlockType=(this.BlockType^this.BlockType)|blockTypeToConvert;
		return this.BlockType;
	}
	void BlockTypeAdd(blockTypes blockTypeToAdd){
		this.BlockType|=blockTypeToAdd;
	}
	void BlockTypeRemove(blockTypes blockType2GetRidOf){
		this.BlockType=(this.BlockType^blockType2GetRidOf)&this.BlockType;
	}
	void BlockTypeSwitch(blockTypes blockType2Switch){
		this.BlockType=(this.BlockType^blockType2Switch);
	}
	void BlockTypeReset(){
		this.BlockType^=this.BlockType;
		this.Entity=-1;
		this.BaseMdlPath[0]=EOS;
	}
}
BM_BlockDataSet Block[MaxBlocks+MAXPLAYERS]; 
void blockData_ResetAllBlocks(){ for(int i=MaxClients+1 ; i<MaxBlocks+MaxClients ; i++) Block[i].BlockTypeReset(); }

