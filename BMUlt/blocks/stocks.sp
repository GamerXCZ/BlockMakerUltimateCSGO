//temp.
int GetAimOriginDist(iClient, Float:hOrigin[3], Float:fDist=100.0) {
	float vAngles[3], fOrigin[3], fFw[3];
	GetClientEyePosition(iClient,fOrigin);
	GetClientEyeAngles(iClient, vAngles);
	GetAngleVectors(vAngles, fFw, NULL_VECTOR, NULL_VECTOR);
	NormalizeVector(fFw, fFw);
	ScaleVector(fFw, fDist);
	
	AddVectors(hOrigin, fOrigin, hOrigin);
	AddVectors(hOrigin, fFw, hOrigin);

	new Handle:trace = TR_TraceRayFilterEx(fOrigin, hOrigin, MASK_SHOT, RayType_EndPoint, trNoPlayers);
	if(TR_DidHit(trace)) {
		TR_GetEndPosition(hOrigin, trace);
		CloseHandle(trace);
		return 1;
	}
	
	CloseHandle(trace);
	return 0;
}

//for later to organize
bool trNoPlayers(int iEnt, int iBitMask, any iData){ return !(1<=iEnt<=MaxClients); }