#define BLOCKNAME_MAXLENGTH 96
enum struct BM_BlockTypesArray {
	char sPath[BLOCKNAME_MAXLENGTH];
	char sName[BLOCKNAME_MAXLENGTH];
	blockTypes Type;
}
ArrayList g_ArrayBlockTypeData = null;
blockTypes g_BlockTypeSum;
blockTypes g_BlockTypeControl = (BLOCK_NONE<<BLOCK_MAX)-BLOCK_NONE;
char g_sBlockResType[6][] = {".mdl", ".vvd", ".phy", ".dx90.vtx", ".vmt", ".vtf" };

void blockTypes_createArray(){ g_ArrayBlockTypeData = new ArrayList(sizeof(BM_BlockTypesArray)); g_BlockTypeSum = BLOCK_NONE;}
void blockTypes_registerToArray(blockTypes blockType, char sBlock[BLOCKNAME_MAXLENGTH], char sPath[BLOCKNAME_MAXLENGTH]){
	if(g_ArrayBlockTypeData == null){
		blockTypes_createArray();
		#if defined LOGCHAT
		LogMessage("⇥ START: BLOCKTYPE REGISTER");
		#endif
	}

	int iSize = GetArraySize(g_ArrayBlockTypeData);
	if(iSize < view_as<int>(BLOCK_MAX)){
		BM_BlockTypesArray blockTypeData;
		blockTypeData.Type=blockType;
		g_BlockTypeSum|=blockType;
		
		FormatEx(blockTypeData.sName, BLOCKNAME_MAXLENGTH, sBlock);
		FormatEx(blockTypeData.sPath, BLOCKNAME_MAXLENGTH, sPath);
		g_ArrayBlockTypeData.PushArray(blockTypeData);
		#if defined LOGCHAT
		LogMessage("✓ REGISTERED BASEMDL PATH %s for BLOCKTYPE %d", sPath, view_as<int>(blockType));
		#endif /* LogMessage(" %d / %d", view_as<int>(g_BlockTypeSum), view_as<int>(g_BlockTypeControl)); */
		if(g_BlockTypeSum^g_BlockTypeControl < BLOCK_NONE){
			#if defined LOGCHAT
			LogMessage("⍻ END: BLOCKTYPE REGISTER. BLOCKTYPE COUNT: %d (bitsum %d/%d)", view_as<int>(BLOCK_MAX), view_as<int>(g_BlockTypeSum), view_as<int>(g_BlockTypeControl));
			#endif
			//w.i.p	blockTypes_precacheArray();
		}
	}
	#if defined LOGCHAT
	else LogError("⛌ Error! UNABLE TO REGISTER BLOCKTYPE (%d) - CHECK YOUR MAX_BLOCKS in blockmakerultimate_blocks.inc", view_as<int>(blockType));
	#endif
}
void blockTypes_precacheArray(){
	char sPath[128];
	int iSize = GetArraySize(g_ArrayBlockTypeData);
	if(iSize>=1){
		#if defined LOGCHAT
		LogMessage("⇥ START: BLOCKTYPE BASEMDL FORMAT&PRECACHE");
		#endif
		BM_BlockTypesArray blockTypeData;
		for(int i=0 ; i < iSize ; i++){
			g_ArrayBlockTypeData.GetArray(i, blockTypeData);
			
			FormatEx(sPath, sizeof sPath, blockTypeData.sPath);
			#if defined LOGCHAT
			LogMessage(sPath);
			#endif
			AddFileToDownloadsTable(sPath);
			PrecacheModel(sPath);
			#if defined LOGCHAT
			LogMessage("✓ PRECACHED AND ADDED %s", sPath);
			#endif
			for(int k = 1 ; k < 4 ; k++){
				ReplaceString(sPath, sizeof sPath, g_sBlockResType[k], g_sBlockResType[k+1]);
				AddFileToDownloadsTable(sPath);
				#if defined LOGCHAT
				LogMessage("✓ ADDED %s", sPath);
				#endif
			}
			ReplaceString(sPath, sizeof sPath, "models", "materials");
			for(int m = 4 ; m < 6 ; m++){
				ReplaceString(sPath, sizeof sPath, g_sBlockResType[m], g_sBlockResType[m+1]);
				AddFileToDownloadsTable(sPath);
				#if defined LOGCHAT
				LogMessage("✓ ADDED %s", sPath);
				#endif
			}	//W.I.P _s 
		}
		#if defined LOGCHAT
		LogMessage("⍻ END: BLOCKTYPE BASEMDL FORMAT&PRECACHE");
		#endif
	}
	#if defined LOGCHAT
	else LogMessage("⌖ Warning! NO BASEMDL MODELS TO PRECACHE FOR CERTAIN BLOCKTYPES");
	#endif
}

void blockTypes_getModelPathByBlockType(blockTypes blockType, char[] sFormatPath, int iLen){
	int iSize = GetArraySize(g_ArrayBlockTypeData);
	if(iSize>=1){
		BM_BlockTypesArray blockTypeData;
		for(int i = 0 ; i<iSize ; i++){
			g_ArrayBlockTypeData.GetArray(i, blockTypeData);
			
			if(blockTypeData.Type==blockType){
				FormatEx(sFormatPath, iLen, blockTypeData.sPath);
				#if defined LOGCHAT
				LogMessage("⌖ BLOCKTYPE %d found BASEMDL PATH: %s", view_as<int>(blockType), sFormatPath);
				#endif
				return;
			}
		}
	}
	#if defined LOGCHAT
	LogMessage("⌖ Warning! BASEMDL PATH NOT FOUND for BLOCKTYPE %d", view_as<int>(blockType));
	#endif
	sFormatPath[0]=EOS;
}