const float TempBlockPlaceDistance = 150.0;

void util_BlockSpawn_AtAim(int iClient, int iToApply, char[] sMdlPath){
	float fPos[3]; GetAimOriginDist(iClient, fPos, 100.0);
	DispatchKeyValue(iToApply, "model", sMdlPath);
	TeleportEntity(iToApply, fPos, view_as<float>({0.0, 0.0, 0.0}), NULL_VECTOR);
	DispatchSpawn(iToApply);
	SetEntityMoveType(iToApply, MOVETYPE_NONE);
	SetEntityRenderMode(iToApply, RENDER_TRANSALPHA);
	SDKHook(iToApply, SDKHook_Touch, _hook_TouchBlock);
}

void util_BlockRemove(int iToDump){
	SDKUnhook(iToDump, SDKHook_Touch, _hook_TouchBlock);
	AcceptEntityInput(iToDump, "Kill");
}

public int _nativeBlockTypeRegister(Handle hPlugin, int iParamNum){
	int iType;
	if((iType=GetNativeCell(1))<(1<<view_as<int>(BLOCK_MAX))){
		char sBlock[BLOCKNAME_MAXLENGTH], sPath[BLOCKNAME_MAXLENGTH];
		GetNativeString(2, sBlock, BLOCKNAME_MAXLENGTH);
		GetNativeString(3, sPath, BLOCKNAME_MAXLENGTH);
		blockTypes_registerToArray(view_as<blockTypes>(iType), sBlock, sPath);
		return iType;
	}
	LogError("BLOCKTYPE %d ERROR. UNABLE TO REGISTER BLOCKTYPE - OUTOFBOUNDS", iType);
	return 0;
}
