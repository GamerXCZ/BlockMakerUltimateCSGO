# BlockMakerUltimateCSGO

**BlockMaker Ultimate CS:GO** a perfected tool for SourceMod that allows you to put interactive, fun entities on any MAP.
Built by CS 1.6 EasyBlock gamemode enthusiasts.

Only your imagination is the limit here.

#### Features
- 3rd party plugin(sub-plugin) support for custom objects;
- Online list with custom objects coded by users for everyone;
- Each object and its effects represents 1 bit value - combine various objects and their effects for max creativity;
- Custom properties - lots of options for each entity;
- GlowFX Animation system to make certain objects eye-catching;
- Will update this feature list very shortly..